﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Serialization
{
  [Serializable]
  class User
  {
    public string dataItemOne = "First data block";
    public string dataItemTwo = "More data";
    [OnSerializing]
    private void OnSerializing(StreamingContext context)
    {
      // Вызывается во время процесса сериализации.
      dataItemOne = dataItemOne.ToUpper();
      dataItemTwo = dataItemTwo.ToUpper();
      Console.WriteLine("[OnSerializing]");
    }

    [OnDeserialized]
    private void OnDeserialized(StreamingContext context)
    {
      // Вызывается по завершении процесса десериализации.
      dataItemOne = dataItemOne.ToLower();
      dataItemTwo = dataItemTwo.ToLower();
      Console.WriteLine("[OnDeserialized]");
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      // объект для сериализации
      var user = new User();
      Console.WriteLine("Объект создан");

      // создаем объект BinaryFormatter
      var formatter = new BinaryFormatter();
      // получаем поток, куда будем записывать сериализованный объект
      using (var fs = new FileStream("User.dat", FileMode.OpenOrCreate))
      {
        formatter.Serialize(fs, user);
        Console.WriteLine("Объект сериализован");
      }

      // десериализация из файла people.dat
      using (FileStream fs = new FileStream("User.dat", FileMode.OpenOrCreate))
      {
        var duser = (User)formatter.Deserialize(fs);
        Console.WriteLine("Объект десериализован");
        Console.WriteLine($"Имя: {duser.dataItemOne} --- Возраст: {duser.dataItemTwo}");
      }

      Console.ReadLine();
    }
  }
}