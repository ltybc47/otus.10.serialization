﻿using LibFromEducation;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace SerializationHomeWork
{
  class Program
  {
    static void Main(string[] args)
    {
      using(FileStream fs = new FileStream("binarFile.dat", FileMode.OpenOrCreate)){
        BinaryFormatter formatter = new BinaryFormatter();
      }

      Box box = new Box();
    }
  }
  [Serializable]
  class Person : ISerializable
  {
    private string _name;

    public Person()
    {

    }
    public int Year { get; set; }

    public string Name { get { return _name; } set { _name = value; } }

    public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      info.AddValue("UserName", _name, typeof(string));
    }
    public Person(SerializationInfo info, StreamingContext context)
    {
      _name = (string)info.GetValue("UserName", typeof(string));
    }
  }

  public class Passport
  {
    public int Number { get; set; }
  }

  public class User : ISerializable
  {
    public User()
    {
    }

    private string _name { get; set; }

    private Passport _passport;

    public Passport Passport
    {
      get { return _passport; }
      set { _passport = value; }
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      info.AddValue("UserName", _name, typeof(string));

    }

    private User(SerializationInfo info, StreamingContext context)
    {
      _name = (string)info.GetValue("UserName", typeof(string));
    }
  }

}
