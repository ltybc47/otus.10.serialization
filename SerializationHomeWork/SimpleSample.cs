﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace test
{
  [Serializable]
  class User
  {
    public string Name { get; set; }
    public int Age { get; set; }

    public User(string name, int age)
    {
      Name = name;
      Age = age;
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      // объект для сериализации
      var user = new User("Tom", 29);
      Console.WriteLine("Объект создан");

      // создаем объект BinaryFormatter
      var formatter = new BinaryFormatter();
      // получаем поток, куда будем записывать сериализованный объект
      using (var fs = new FileStream("User.dat", FileMode.OpenOrCreate))
      {
        formatter.Serialize(fs, user);
        Console.WriteLine("Объект сериализован");
      }

      // десериализация из файла people.dat
      using (FileStream fs = new FileStream("User.dat", FileMode.OpenOrCreate))
      {
        var duser = (User)formatter.Deserialize(fs);
        Console.WriteLine("Объект десериализован");
        Console.WriteLine($"Имя: {duser.Name} --- Возраст: {duser.Age}");
      }

      Console.ReadLine();
    }
  }
}